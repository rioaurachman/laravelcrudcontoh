@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3">
    <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">edit post {{$posts->id}}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="/posts/{{$posts->id}}" method="POST">
              @csrf
              @method('PUT')
              <div class="box-body">
                <div class="form-group">
                  <label for="title">Title</label>
                  <input type="text" class="form-control" id="title" name="title" value="{{old('title',$posts->title)}}" placeholder="Enter Title">
                  @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="body">Body</label>
                  <input type="text" class="form-control" id="body" name="body" value="{{old('body',$posts->body)}}" placeholder="Body">
                  @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Create</button>
              </div>
            </form>
    </div>
</div>
@endsection