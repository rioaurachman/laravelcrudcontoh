@extends('adminlte.master')

@section('content')
    <div class="mt-3 ml-3">
    <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Posts</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             @if(session('success'))
                <div class="alert alert-sucess">
                    {{session('success')}}
                </div>
             @endif
             <a button class="btn btn btn-primary mb-2" href="/posts/create">Create New Post</a>
              <table class="table table-bordered">
                <thead><tr>
                  <th style="width: 10px">#</th>
                  <th>Title</th>
                  <th>Body</th>
                  <th style="width: 40px">Label</th>
                </tr>
    
              </thead>
              <tbody>
                @forelse($posts as $key =>$post)
                  <tr>
                    <td> {{$key + 1}} </td>
                    <td> {{$post->title}} </td>
                    <td> {{$post->body}} </td>
                    <td style="display: flex;">
                        <a href="/posts/{{$post->id}}" class="btn btn-info btn-sm">show</a>
                        <a href="/posts/{{$post->id}}/edit" class="btn btn-info btn-sm">edit</a>
                        <form action="/posts/{{$post->id}}" method="post">
                           @csrf
                           @method('DELETE')
                           <input type="submit" value="delete" class="btn btn-danger btn-sm"> 
                        </form>
                    
                    </td>
                  </tr>
                @empty

                    <tr>
                    <p>No Posts</p>
                    </tr>
                @endforelse

              </tbody>
              </table>
            </div>
            <!-- /.box-body -->

          </div>
    </div>
@endsection