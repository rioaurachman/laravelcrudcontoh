<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/lain', function () {
    return view('welcome');
});

Route::get('/posts/create','PostController@create');
Route::post('/posts','PostController@store');
Route::get('/posts','PostController@index');
Route::get('/posts/{id}','PostController@show');
Route::get('/posts/{id}/edit','PostController@edit');
Route::put('/posts/{id}/','PostController@update');
Route::delete('/posts/{id}/','PostController@destroy');



Route::get('/master', function () {
    return view('adminlte.master');
});

Route::get('/', function () {
    return view('1');
});

Route::get('/data-tables', function () {
    return view('2');
});

Route::get('/items', function () {
    return view('items.index');
});

Route::get('/items/create', function () {
    return view('items.create');
});